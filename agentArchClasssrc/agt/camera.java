import jason.asSyntax.*;
import jason.*;
import jason.asSemantics.*;
import jason.architecture.*;
import java.util.*;
import java.io.*;
import org.opencv.core.Core;
import org.opencv.core.*;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs; 
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractorMOG2;
import org.opencv.video.Video;

//Apache jena 
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.*;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.Resource;

import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.reasoner.ValidityReport;
import org.apache.jena.reasoner.ValidityReport.Report;
import org.apache.jena.util.FileManager;

/*
Problem:
1. Jason parser cannot create literal with ont string format
2. Identify type of output from opencv - contours
3. Create Individual name for all instances to be added to model
*/

public class camera extends AgArch {
    final int size_threshold = 1000;
    BackgroundSubtractorMOG2 fgbg;
    //Modify to reflect location of the file
    String baseLoc = "./rsc/easy/set1/";

    //Ontology
    Model tbox, abox;
    Reasoner reasoner;
    OntModelSpec instances;
    OntModel m;
    String sourceURL = "http://www.semanticweb.org/ludwig/ontologies/disTrac";
    String namespace = sourceURL + "#";
    int cycle;

    @Override
    public void init(){
        
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        fgbg = Video.createBackgroundSubtractorMOG2();
        FileManager.get().addLocatorClassLoader(camera.class.getClassLoader());
        
        tbox = FileManager.get().loadModel("rsc/onto/nOnto.owl", null, "RDF/XML"); // http://en.wikipedia.org/wiki/Tbox
        reasoner = ReasonerRegistry.getOWLReasoner().bindSchema(tbox.getGraph());
        
        instances = new OntModelSpec( OntModelSpec.OWL_DL_MEM );
        instances.setReasoner( reasoner );
        cycle = 1;
    }


    @Override
    public void act(ActionExec action){
        String functor = action.getActionTerm().getFunctor();
        if (functor.equals("countP")){
            
            //Create new temporary model to hold changes
            //Unique in each cycle - temporary
            abox = ModelFactory.createDefaultModel();
            m = ModelFactory.createOntologyModel( instances, abox );


            Unifier u = action.getIntention().peek().getUnif();
            int length = action.getActionTerm().getTermsArray().length;
            Term[] args = new Term[length];
            for (int i = 0; i < args.length; i++) {
                args[i] = action.getActionTerm().getTerm(i).capply(u);
            }
            String fileLoc = "";
            try{
                fileLoc = baseLoc + getTS().getUserAgArch().getAgName()
                                + "/cam_" + String.format("%05d", (int)(((NumberTerm)args[0]).solve())) + ".jpg";
            }catch(Exception e){
                e.printStackTrace();
            }
            ///*
            //Classes
            OntClass track = m.getOntClass(namespace + "Track");
            OntClass trackSS = m.getOntClass(namespace + "TrackSnapshot");
            OntClass x = m.getOntClass(namespace + "X_Pos");
            OntClass y = m.getOntClass(namespace + "Y_Pos");
            OntClass br = m.getOntClass(namespace + "BottomRight");
            OntClass tl = m.getOntClass(namespace + "TopLeft");
            OntClass rect = m.getOntClass(namespace + "BoundingRectangle");
            OntClass frame = m.getOntClass(namespace + "Frame");
            OntClass h = m.getOntClass(namespace + "Height");
            OntClass w = m.getOntClass(namespace + "Width");
            OntClass source = m.getOntClass(namespace + "Source");

            //Object property 
            ObjectProperty hxc= m.getObjectProperty(namespace + "hasXCoordinate");
            ObjectProperty hyc= m.getObjectProperty(namespace + "hasYCoordinate");
            ObjectProperty hbr = m.getObjectProperty(namespace + "hasBottomRightPoint");
            ObjectProperty htl = m.getObjectProperty(namespace + "hasTopLeftPoint");
            ObjectProperty hbRect = m.getObjectProperty(namespace + "hasBoundingRect");
            ObjectProperty iFrame = m.getObjectProperty(namespace + "inFrame");
            ObjectProperty hss = m.getObjectProperty(namespace + "hasSnapshot");
            ObjectProperty fSource = m.getObjectProperty(namespace + "fromSource");

            //Datatype property
            DatatypeProperty val = m.getDatatypeProperty(namespace + "hasValue");    
            DatatypeProperty height = m.getDatatypeProperty(namespace + "hasHeight");
            DatatypeProperty width = m.getDatatypeProperty(namespace + "hasWidth");
            DatatypeProperty filen = m.getDatatypeProperty(namespace + "hasFilename");

            String agentID = getTS().getUserAgArch().getAgName();
            String baseID = getTS().getUserAgArch().getAgName() + "_" + cycle;
            Individual cFrame = m.createIndividual(namespace + "frame_" + baseID , frame);
            Individual cSource = m.createIndividual(namespace + "source_" + agentID, source);
            m.add(cFrame, fSource, cSource);
            m.add(cSource, filen, m.createTypedLiteral(new String(fileLoc)));
            //*/
            ListTerm result = new ListTermImpl();
            Iterator<MatOfPoint> iterator = countP(fileLoc).iterator();
            int curIt = 0;
            while (iterator.hasNext()){

                MatOfPoint contour = iterator.next();
                double area = Imgproc.contourArea(contour);
                if(area >= size_threshold){
                    
                    curIt += 1;
                    Rect r = Imgproc.boundingRect(contour);
                    
                    Individual botR = m.createIndividual(namespace + "br_" + baseID + "_" + curIt, br);
                    Individual topL = m.createIndividual(namespace + "tl_" + baseID + "_" + curIt, tl);
                    Individual topX = m.createIndividual(namespace + "tx_" + baseID + "_" + curIt, x);
                    Individual topY = m.createIndividual(namespace + "ty_" + baseID + "_" + curIt, y);
                    Individual botX = m.createIndividual(namespace + "bx_" + baseID + "_" + curIt, x);
                    Individual botY = m.createIndividual(namespace + "by_" + baseID + "_" + curIt, y);
                    Individual boundR = m.createIndividual(namespace + "brect_" + baseID + "_" + curIt, rect);
                    m.add(topL, hxc, topX);
                    m.add(topL, hyc, topY);
                    m.add(topX, val, m.createTypedLiteral(new Double(r.x)));
                    m.add(topY, val, m.createTypedLiteral(new Double(r.y)));
                    
                    m.add(botX, val, m.createTypedLiteral(new Double(r.x+r.width)));
                    m.add(botY, val, m.createTypedLiteral(new Double(r.y+r.height)));
                    m.add(botR, hxc, botX);
                    m.add(botR, hyc, botY);
                    
                    m.add(boundR, hbr, botR);
                    m.add(boundR, htl, topL);
                    m.add(cFrame, hbRect, boundR);
                    m.add(boundR, height, m.createTypedLiteral(new Double(r.height)));
                    m.add(boundR, width, m.createTypedLiteral(new Double(r.width)));
                }
            }
            ///*
            //Transform model to string  //
            StringWriter out = new StringWriter();
            m.write(out, "N-TRIPLES");
            String rstring = out.toString();
            String[] iString = rstring.split("\n");
            for (String i: iString){
                result.add(new StringTermImpl( (i.split("\n")[0]).replaceAll("\"", "PP")));
            }
            //*/
            //Send model as string over
            action.setResult(u.unifiesNoUndo(args[1], result));
            actionExecuted(action);
            cycle += 1;
            return;
        }else{
            super.act(action);
            cycle += 1;
        }
    }

    public List<MatOfPoint> countP(String location){
        int con = 0;
        Mat frame =  Imgcodecs.imread(location, Imgcodecs.CV_LOAD_IMAGE_COLOR);
        Mat processedFrame = new Mat();
        fgbg.apply(frame, processedFrame);
        Imgproc.threshold(processedFrame, processedFrame, 128, 255, Imgproc.THRESH_BINARY);
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(processedFrame,contours,new Mat(),Imgproc.RETR_TREE,Imgproc.CHAIN_APPROX_SIMPLE);
        return contours;
    }
}
