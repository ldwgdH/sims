import jason.asSyntax.*;
import jason.*;
import jason.asSemantics.*;
import jason.architecture.*;
import java.util.*;
import org.opencv.core.Core;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs; 
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractorMOG2;
import org.opencv.video.Video;

//Apache jena 
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.ontology.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.reasoner.ValidityReport;
import org.apache.jena.reasoner.ValidityReport.Report;
import org.apache.jena.util.FileManager;
import org.apache.jena.util.iterator.ExtendedIterator;
import java.nio.file.*;
import java.io.IOException;
import org.apache.commons.io.IOUtils;

public class context extends AgArch {
    //Modify to reflect location of the file
    String baseLoc = "./rsc/easy/set1/";
    String sourceURL = "http://www.semanticweb.org/ludwig/ontologies/disTrac";
    String namespace = sourceURL + "#";
    Model tbox, abox;
    Model tmp;
    OntModelSpec instances;
    InfModel inf;
    Reasoner reasoner;

    @Override
    public void init(){
        
        FileManager.get().addLocatorClassLoader(camera.class.getClassLoader());
        
        tbox = FileManager.get().loadModel("rsc/onto/nOnto.owl", null, "RDF/XML"); // http://en.wikipedia.org/wiki/Tbox
        reasoner = ReasonerRegistry.getOWLReasoner().bindSchema(tbox.getGraph());
        abox = ModelFactory.createDefaultModel();
        //_RDFS_INF
        instances = new OntModelSpec( OntModelSpec.OWL_DL_MEM );
        instances.setReasoner( reasoner );
        inf = ModelFactory.createInfModel(reasoner, abox);
    }

    /*
    React to new model:
    For a in listTerm:
        parse(a);
        if a.predicate in tbox.predicate:
            model.add(subject, predicate, object)
        else:
            register error

    */
    /*
    public Model tripleToModel(String triples){

        return new Model();
    }
    */
    @Override
    public void act(ActionExec action){
        String functor = action.getActionTerm().getFunctor();
        if (functor.equals("update")){
            Unifier u = action.getIntention().peek().getUnif();
            int length = action.getActionTerm().getTermsArray().length;
            Term[] args = new Term[length];
            for (int i = 0; i < args.length; i++) {
                args[i] = action.getActionTerm().getTerm(i).capply(u);
            }
            try{
                NumberTerm fileLoc = (NumberTerm) args[0];
                ListTerm result = (ListTerm) args[1];
                String triples = "";
                
                //Process input to union model
                tmp = ModelFactory.createDefaultModel();
                ///*
                Model m = ModelFactory.createOntologyModel( instances, tmp );
                for (Term t: result){
                    triples = triples + ((StringTerm) t).getString();
                }
                //System.out.println(triples.replaceAll("PP", "\""));
                //*/
                //Add model to abox
                //abox.add(m);


                Atom agent = (Atom) args[2];
                m.read(IOUtils.toInputStream(triples.replaceAll("PP", "\""), "UTF-8"),
                    null, "N-TRIPLES");
                String text = "";
                //String text = "./rsc/easy/set1/" + agent.toString() + "/cam_" + String.format("%05d", (int) (fileLoc.solve())) +".jpg,";
                ///*
                StmtIterator frames = m.listStatements(null, tbox.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), (RDFNode)m.getResource("http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame"));
                while (frames.hasNext()){
                    Statement cFrame = frames.nextStatement();
                    StmtIterator sources = m.listStatements(cFrame.getSubject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource"), (RDFNode)null);
                    while(sources.hasNext()){
                        Statement cSource = sources.nextStatement();
                        StmtIterator filenames = m.listStatements((Resource)cSource.getObject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename"), (RDFNode)null);
                        while(filenames.hasNext()){
                            Statement filename = filenames.nextStatement();
                            text = text + filename.getObject().toString() + ",";
                        }
                    }
                }

                StmtIterator rects = m.listStatements(null, tbox.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), (RDFNode)m.getResource("http://www.semanticweb.org/ludwig/ontologies/disTrac#BoundingRectangle"));
                while (rects.hasNext()){
                    Statement cRect = rects.nextStatement();
                    Statement TLpoint = m.listStatements(cRect.getSubject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#hasTopLeftPoint"), (RDFNode)null).nextStatement();
                    Statement BRpoint = m.listStatements(cRect.getSubject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#hasBottomRightPoint"), (RDFNode)null).nextStatement();
                    Statement Tx = m.listStatements((Resource)TLpoint.getObject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#hasXCoordinate"), (RDFNode)null).nextStatement();
                    Statement Ty = m.listStatements((Resource)TLpoint.getObject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#hasYCoordinate"), (RDFNode)null).nextStatement();
                    Statement Bx = m.listStatements((Resource)BRpoint.getObject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#hasXCoordinate"), (RDFNode)null).nextStatement();
                    Statement By = m.listStatements((Resource)BRpoint.getObject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#hasYCoordinate"), (RDFNode)null).nextStatement();
                    Statement x1 = m.listStatements((Resource)Tx.getObject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#hasValue"), (RDFNode)null).nextStatement();
                    Statement y1 = m.listStatements((Resource)Ty.getObject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#hasValue"), (RDFNode)null).nextStatement();
                    Statement x2 = m.listStatements((Resource)Bx.getObject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#hasValue"), (RDFNode)null).nextStatement();
                    Statement y2 = m.listStatements((Resource)By.getObject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#hasValue"), (RDFNode)null).nextStatement();
                    
                    text = text + ((org.apache.jena.rdf.model.Literal)x1.getObject()).getValue() + "," + ((org.apache.jena.rdf.model.Literal)y1.getObject()).getValue() + "," + ((org.apache.jena.rdf.model.Literal)x2.getObject()).getValue() + "," + ((org.apache.jena.rdf.model.Literal)y2.getObject()).getValue() + ",";
                }
                /*
                for (Term t: result){
                    double n = ((NumberTerm)t).solve();
                    text = text + n + ',';
                }
                */
                // Write to file
                
                text = text + "\n";
                try {
                    Files.write(Paths.get(agent.toString()), text.getBytes(), StandardOpenOption.APPEND);
                }catch (IOException e) {
                    e.printStackTrace();
                }
                
                action.setResult(true);
                actionExecuted(action);
            }catch(Exception e){
                e.printStackTrace();
            }

        }else if(functor.equals("update")){
            
        }else{
            super.act(action);
        }
    }



}
