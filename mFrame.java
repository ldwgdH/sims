import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;
import javax.swing.*;
import org.opencv.core.Core;
import org.opencv.core.*;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs; 
import org.opencv.imgproc.Imgproc;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/*
This update the frame as soon as the entry is found in the file, 
the update does not care whether or not all camera has updated the frame, 
hence is not coordinated or calibrated.
*/
public class mFrame extends JFrame{
    public mPanel panel_1 = new mPanel(1);
    public mPanel panel_2 = new mPanel(2);
    public mPanel panel_3 = new mPanel(3);
    
    public mFrame(){
        super("Content");

        JPanel main_panel = new JPanel();
        main_panel.setLayout(new GridLayout(2,2));
        main_panel.add(panel_1);
        main_panel.add(panel_2);
        main_panel.add(panel_3);
        add(main_panel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(900,700);
    }
    
    public static void main(String arg[]){
        mFrame m = new mFrame();
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        try{
            FileInputStream fstream1 = new FileInputStream("cam_1");
            BufferedReader br1 = new BufferedReader(new InputStreamReader(fstream1));
            FileInputStream fstream2 = new FileInputStream("cam_2");
            BufferedReader br2 = new BufferedReader(new InputStreamReader(fstream2));
            FileInputStream fstream3 = new FileInputStream("cam_3");
            BufferedReader br3 = new BufferedReader(new InputStreamReader(fstream3));            
            
            String[] strLine = new String[3];
            while (true){
                Boolean l1, l2, l3 = false;
                if ((strLine[0] = br1.readLine()) != null){
                    l1 = true;
                    String [] result = strLine[0].split(",");
                    Mat frame =  Imgcodecs.imread(result[0], Imgcodecs.CV_LOAD_IMAGE_COLOR);
                    if (result.length > 1){
                        int nIterate = (result.length - 1)/4;
                        for (int a=0; a<nIterate; a++){
                            double x1, y1, x2, y2;
                            x1 = Double.parseDouble(result[a*4+1]);
                            y1 = Double.parseDouble(result[a*4+2]);
                            x2 = Double.parseDouble(result[a*4+3]);
                            y2 = Double.parseDouble(result[a*4+4]);
                            Imgproc.rectangle(frame, new Point(x1, y1), new Point(x2, y2), new Scalar(0, 255, 0), 2);
                        }
                    }
                    
                    //Point = (x, y)
                    //2 Point for each rectangle
                    //List of 2 Point for each frame
                    //frame_1, [p1, p2],[],[],[]
                    //frame = processFrame(frame, strLine[0].split(",")[1]);

                    //
                    m.panel_1.refresh(frame);
                }
                if ((strLine[1] = br2.readLine()) != null){
                    l2 = true;
                    String [] result = strLine[1].split(",");
                    Mat frame =  Imgcodecs.imread(result[0], Imgcodecs.CV_LOAD_IMAGE_COLOR);
                    if (result.length > 1){
                        int nIterate = (result.length - 1)/4;
                        for (int a=0; a<nIterate; a++){
                            double x1, y1, x2, y2;
                            x1 = Double.parseDouble(result[a*4+1]);
                            y1 = Double.parseDouble(result[a*4+2]);
                            x2 = Double.parseDouble(result[a*4+3]);
                            y2 = Double.parseDouble(result[a*4+4]);
                            Imgproc.rectangle(frame, new Point(x1, y1), new Point(x2, y2), new Scalar(0, 255, 0), 2);
                        }
                    }
                    m.panel_2.refresh(frame);
                }
                if ((strLine[2] = br3.readLine()) != null){
                    l3 = true;
                    String [] result = strLine[2].split(",");
                    Mat frame =  Imgcodecs.imread(result[0], Imgcodecs.CV_LOAD_IMAGE_COLOR);
                    if (result.length > 1){
                        int nIterate = (result.length - 1)/4;
                        for (int a=0; a<nIterate; a++){
                            double x1, y1, x2, y2;
                            x1 = Double.parseDouble(result[a*4+1]);
                            y1 = Double.parseDouble(result[a*4+2]);
                            x2 = Double.parseDouble(result[a*4+3]);
                            y2 = Double.parseDouble(result[a*4+4]);
                            Imgproc.rectangle(frame, new Point(x1, y1), new Point(x2, y2), new Scalar(0, 255, 0), 2);
                        }
                    }
                    m.panel_3.refresh(frame);
                }
                
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}