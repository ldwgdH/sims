package external;

import jason.asSyntax.*;
import jason.*;
import jason.asSemantics.*;
import java.nio.file.*;
import java.io.IOException;

public class update extends DefaultInternalAction{
@Override
    public Object execute(TransitionSystem ts, final Unifier un, final Term[] args)
    throws Exception{
        try{
            NumberTerm fileLoc = (NumberTerm) args[0];
            //NumberTerm result = (NumberTerm) args[1];
            ListTerm result = (ListTerm) args[1];
            Atom agent = (Atom) args[2];
            String text = "./rsc/easy/set1/" + agent.toString() + "/cam_" + String.format("%05d", (int) (fileLoc.solve())) +".jpg,";
            
            for (Term t: result){
                double n = ((NumberTerm)t).solve();
                text = text + n + ',';
            }
            
            text = text + "\n";
            try {
                Files.write(Paths.get(agent.toString()), text.getBytes(), StandardOpenOption.APPEND);
            }catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }catch(Exception e){
            throw new JasonException("Error HERE");
        }
    }
}