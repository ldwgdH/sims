package external;

import jason.asSyntax.*;
import jason.*;
import jason.asSemantics.*;

public class time extends DefaultInternalAction{
@Override
    public Object execute(TransitionSystem ts, final Unifier un, final Term[] args)
    throws Exception{
        try{
            un.unifies(args[0], new StringTermImpl(""+System.currentTimeMillis()));
            return true;
        }catch(Exception e){
            throw new JasonException("Error HERE");
        }
    }
}