/* Initial beliefs and rules */
frame(0).
max_frame(1551).
/* Initial goals */

!start.

/* Plans */

+!start : true <- .print("Camera agent started");
                    .wait(1000); 
                    .my_name(N);
                    .send(contextAgent, tell, joined(N)).

+sim_start <- .println("Requested to start simulation from context agent");
                ?frame(N); 
                .println("Frame: ", N);
                !sim(N). 
                

+!sim(N) : max_frame(M) & M>=N <-
            .print("Processing frame: ", N);
            countP(N, Result); 
            .send(contextAgent, tell, processed(N,Result));
            -+frame(N+1);
            !sim(N+1).

+!sim(N) : max_frame(M) & N>M <-
            .println("Finished processing");
            .send(contextAgent, tell, finished).

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
//{ include("$moiseJar/asl/org-obedient.asl") }
