/* Initial beliefs and rules */
num_agent(0).
total_agent(3).
/* Initial goals */

!start.
!simulate.

/* Plans */

+!start : true <- .print("Context agent started").

+joined(X) <- .println(X, " joined the context"); !calculate.

+!calculate <- .count(joined(_), N);
                .println("Currently connected camera agent: ", N);
                -+num_agent(N).

+!simulate: num_agent(M) & total_agent(N) & M>=N 
                <- .println("Starting simulation now");
                external.time(T);
                .println("***", T);
                .findall(Name, joined(Name), Names);
                .send(Names, tell, sim_start).

+!simulate: num_agent(M) & total_agent(N) & M<N <- 
                .println("Not starting simulation"); .wait(1000); !simulate.

+processed(X,Y)[source(A)] <- 
            external.time(T);
            .println("***", X, " - ", T);
            update(X,Y,A).

+finished[source(A)]<- 
            external.time(T);
            .println("***", T);
            .print(A, " has finished").
            

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
//{ include("$moiseJar/asl/org-obedient.asl") }
