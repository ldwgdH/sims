import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.FileManager;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.*;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import java.io.*;
import org.apache.commons.io.IOUtils;
import org.apache.jena.graph.NodeFactory;

import org.apache.jena.util.iterator.*;

public class ontotest2{
    public static void main(String[] args) throws java.io.IOException{

        Model tbox, abox;
        OntModelSpec instances;
        Reasoner reasoner;

        tbox = FileManager.get().loadModel("nOnto.owl", null, "RDF/XML"); // http://en.wikipedia.org/wiki/Tbox
        reasoner = ReasonerRegistry.getOWLReasoner().bindSchema(tbox.getGraph());
        //_RDFS_INF
        instances = new OntModelSpec( OntModelSpec.OWL_DL_MEM_RDFS_INF );

        String triples = "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_1> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00000.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_1> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_1> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_1> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00000.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_1> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_1> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_1> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_1> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_1> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00000.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_2> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00001.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_2> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_2> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_2> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_2> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_2> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_2> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_2> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_2> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00001.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_2> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_2> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00001.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_2> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_2> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_2> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_2> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_3> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_3> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_3> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_3> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00002.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_3> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_3> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00002.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_3> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_3> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_3> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_3> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_3> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00002.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_3> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_3> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_3> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_3> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_4> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00003.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_4> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_4> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_4> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_4> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_4> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00003.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_4> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_4> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_4> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_4> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_4> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_4> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_4> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_4> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00003.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_4> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_5> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00004.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_5> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_5> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_5> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_5> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_5> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_5> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_5> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_5> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00004.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_5> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_5> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00004.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_5> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_5> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_5> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_5> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_6> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_6> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_6> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_6> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00005.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_6> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_6> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00005.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_6> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_6> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_6> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_6> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_6> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00005.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_6> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_6> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_6> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_6> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_7> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00006.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_7> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_7> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_7> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_7> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_7> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00006.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_7> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_7> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_7> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_7> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_7> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_7> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_7> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_7> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00006.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_7> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_8> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00007.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_8> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_8> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_1_8> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_8> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_8> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_8> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_8> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_8> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00007.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_8> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_8> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_8> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_8> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_8> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00007.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_8> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_9> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_9> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_9> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_9> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00008.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_2_9> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_9> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00008.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_9> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Source> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_9> <http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource> <http://www.semanticweb.org/ludwig/ontologies/disTrac#source_cam_3_9> .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_9> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";

        
        
        
        
        /*
        "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_1> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00000.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_1> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00000.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_1> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00000.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_2> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00001.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_2> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_2> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00001.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_2> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_2> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00001.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_2> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_3> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00002.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_3> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_3> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00002.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_3> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_3> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00002.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_3> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_4> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00003.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_4> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_4> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00003.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_4> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_4> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00003.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_4> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_5> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00004.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_5> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_5> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00004.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_5> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_5> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00004.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_5> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_6> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00005.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_6> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_6> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_3/cam_00005.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_3_6> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_6> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_2/cam_00005.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_2_6> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> . \n";
triples += "<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_7> <http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename> \"./rsc/easy/set1/cam_1/cam_00006.jpg\" .<http://www.semanticweb.org/ludwig/ontologies/disTrac#frame_cam_1_7> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame> .";
 */       
        OntModel m;
        abox = ModelFactory.createDefaultModel();
        m = ModelFactory.createOntologyModel( instances, abox );
        
        m.read(IOUtils.toInputStream(triples, "UTF-8"),
                        null, "N-TRIPLES");
        System.out.println(m.size());
        
        //Resource toSearch = ResourceFactory.createResource("<http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame>");
        //System.out.println(m.containsResource(toSearch));
        //System.out.println(m.getResource("<http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame>"));
        //Resource toSearch = ResourceFactory.createResource("<http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame>");
        //System.out.println(tbox.containsResource(toSearch)); 
        
        StmtIterator nn = m.listStatements(null, tbox.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), (RDFNode)m.getResource("http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame"));
        //StmtIterator nn = m.listStatements(null, null, (RDFNode)null);
        while(nn.hasNext()){
            Statement a = nn.nextStatement();
            //System.out.println(a.toString());
            //if (a.getObject().toString().endsWith("Frame")){
                //System.out.println("__"+a.getSubject().toString());
                //System.out.println("__"+a.getSubject().isURIResource());
                StmtIterator n2 = m.listStatements(a.getSubject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#fromSource"), (RDFNode)null);
                while(n2.hasNext()){
                    Statement b = n2.nextStatement();
                    StmtIterator n3 = m.listStatements((Resource)b.getObject(), m.getProperty("http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename"), (RDFNode)null);
                    while(n3.hasNext()){
                        Statement c = n3.nextStatement();
                        System.out.println(c.getObject());
                    }
                    
                }
        }
        
        
        
        /*
        NodeIterator nn = m.listObjects();
        while(nn.hasNext()){
            RDFNode a = nn.nextNode();
            System.out.println(a.toString());
        }
        */
        //Node n = NodeFactory().createURI("<http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame>");
        /*
        Resource r = m.getResource("<http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame>");
        System.out.println(r.toString());
        StmtIterator nn = m.listStatements(null, m.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), (RDFNode)null);
        while(nn.hasNext()){
            Statement a = nn.nextStatement();
            Boolean b = a.getObject() == r;
            if(b){System.out.println("Here");}
            else{System.out.println("THERE");}
            //System.out.println(a.toString());
        }
        */
        /*
        OntProperty nome = m.getOntProperty( "<http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename>" );
        OntClass equipe = m.getOntClass( "<http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame>" );
        ExtendedIterator<? extends OntResource> individuals = equipe.listInstances();
        while(individuals.hasNext()){
            OntResource ee = individuals.next();
            System.out.println( "Equipe instance: " + ee.getProperty( nome ).getString() );
        }
        */
        StmtIterator blockIt = m.listStatements(null, m.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), m.getResource("<http://www.semanticweb.org/ludwig/ontologies/disTrac#Frame>")); 
        //System.out.println(blockIt.length());
        while (blockIt.hasNext()) {
            System.out.println("HERE");
            Statement frame = blockIt.nextStatement();
            // select all statements that have the current Parking entity as subject
            StmtIterator it = m.listStatements(frame.getSubject(), m.getProperty("<http://www.semanticweb.org/ludwig/ontologies/disTrac#hasFilename>"), (RDFNode) null);
            while (it.hasNext()) {
                Statement ii = it.nextStatement();
                System.out.println(ii.getSubject().toString());
                // here you will get all triples for the current Parking block
            }
        }

    }
}