import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.FileManager;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.*;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import java.io.*;

import java.util.Iterator;

public class ontotest {

    public static void main(String[] args) throws java.io.IOException{
        String sourceURL = "http://www.semanticweb.org/ludwig/ontologies/disTrack";
        String namespace = sourceURL + "#";
        FileManager.get().addLocatorClassLoader(ontotest.class.getClassLoader());
        Model schema = FileManager.get().loadModel("disTrackOnto.owl", null, "RDF/XML");
        
        Reasoner reasoner = ReasonerRegistry.getOWLReasoner().bindSchema(schema.getGraph());
        
        Model abox = ModelFactory.createDefaultModel();
        OntModelSpec instances = new OntModelSpec( OntModelSpec.OWL_MEM_RULE_INF );
        instances.setReasoner( reasoner );
        OntModel m = ModelFactory.createOntologyModel( instances, abox );

        /*
        OntModel dis = ModelFactory.createOntologyModel( OntModelSpec.OWL_DL_MEM_RULE_INF, model );
        */
        OntClass trackSSClass = m.getOntClass(namespace + "TrackSnapshot");
            OntClass propertyClass = m.getOntClass(namespace + "Property");
            ObjectProperty hasProperty = m.getObjectProperty(namespace + "hasProperty");
        OntClass locationClass = m.getOntClass( namespace + "Location" );
        System.out.println("HERE1");
        Individual Paris = m. createIndividual ( namespace + "Paris", locationClass);
        System.out.println("HERE2");
        for (int a = 0; a<3; a++){
        Individual tsn = m.createIndividual(namespace + "TrackSnapshot" + a, trackSSClass);
                    Individual ppt = m.createIndividual(namespace + "Property" + a, propertyClass);
                    m.add(tsn, hasProperty, ppt);}
        System.out.println("---- Assertions in the data ----");
        for (Iterator<Resource> i = Paris.listRDFTypes(false); i.hasNext(); ) {
            System.out.println( Paris.getURI() + " is a " + i.next() );
        }

        System.out.println("\n---- Inferred assertions ----");
        Paris = m.getIndividual( namespace + "Paris" );
        for (Iterator<Resource> i = Paris.listRDFTypes(false); i.hasNext(); ) {
            System.out.println( Paris.getURI() + " is a " + i.next() );
        }
        String fileName = "your_file_name_here1.owl";
        FileWriter out = new FileWriter( fileName );
        try {
            m.write( out, "N-TRIPLE" );
            m.write(System.out, "N-TRIPLE");
            //model.read(new ByteArrayInputStream(modelText.getBytes()), null);
            //m.read( in, "N-TRIPLE");
        }
        finally {
        try {
            out.close();
        }
        catch (IOException closeException) {
            // ignore
        }
        }
        
        //StmtIterator iter = model.listStatements();
/*         try {
 *             while ( iter.hasNext() ) {
 *                 Statement stmt = iter.next();
 *                 
 *                 Resource s = stmt.getSubject();
 *                 Resource p = stmt.getPredicate();
 *                 RDFNode o = stmt.getObject();
 *                 
 *                 if ( s.isURIResource() ) {
 *                     System.out.print("URI");
 *                 } else if ( s.isAnon() ) {
 *                     System.out.print("blank");
 *                 }
 *                 
 *                 if ( p.isURIResource() ) 
 *                     System.out.print(" URI ");
 *                 
 *                 if ( o.isURIResource() ) {
 *                     System.out.print("URI");
 *                 } else if ( o.isAnon() ) {
 *                     System.out.print("blank");
 *                 } else if ( o.isLiteral() ) {
 *                     System.out.print("literal");
 *                 }
 *                 
 *                 System.out.println();                
 *             }
 *         } finally {
 *             if ( iter != null ) iter.close();
 *         }
 */
    }

}