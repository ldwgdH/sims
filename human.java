import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.opencv.core.Core;
import org.opencv.core.*;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs; 
import org.opencv.imgproc.Imgproc;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/*
This update the frame as soon as the entry is found in the file, 
the update does not care whether or not all camera has updated the frame, 
hence is not coordinated or calibrated.
*/
public class human extends JFrame{
    public mPanel panel_1 = new mPanel(1);
    public mPanel panel_2 = new mPanel(2);
    public mPanel panel_3 = new mPanel(3);
    public JButton a = new JButton("Seen");
    public static int frame = 0;
    public human(){
        super("Content");

        JPanel main_panel = new JPanel();
        main_panel.setLayout(new GridLayout(2,2));
        main_panel.add(panel_1);
        main_panel.add(panel_2);
        main_panel.add(panel_3);
        main_panel.add(a);
        add(main_panel);
        a.addActionListener(new ActionListener() { 
        public void actionPerformed(ActionEvent e) { 
            System.out.println(frame);
            } 
        });

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(900,700);
    }

    
    public static void main(String arg[]){
        String baseLoc = "./rsc/easy/set2/";
        human m = new human();
        

        

        
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        try{
            System.in.read();
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            
            String fileName1, fileName2, fileName3;


            while (frame<=1551){
                fileName1 = baseLoc + "cam_1" + "/cam_" + String.format("%05d", frame) + ".jpg";
                fileName2 = baseLoc + "cam_2" + "/cam_" + String.format("%05d", frame) + ".jpg";
                fileName3 = baseLoc + "cam_3" + "/cam_" + String.format("%05d", frame) + ".jpg";
                Mat frame1 =  Imgcodecs.imread(fileName1, Imgcodecs.CV_LOAD_IMAGE_COLOR);
                Mat frame2 =  Imgcodecs.imread(fileName2, Imgcodecs.CV_LOAD_IMAGE_COLOR);
                Mat frame3 =  Imgcodecs.imread(fileName3, Imgcodecs.CV_LOAD_IMAGE_COLOR);
                m.panel_1.refresh(frame1);
                m.panel_2.refresh(frame2);
                m.panel_3.refresh(frame3);
                frame +=1;
                Thread.sleep(10);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}