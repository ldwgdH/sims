import java.awt.*;  
import java.awt.image.BufferedImage;  
import java.awt.image.DataBufferByte;  
import javax.swing.*;  
import javax.swing.border.TitledBorder;
import org.opencv.core.Core;  
import org.opencv.core.Mat;  

//Similar to udallascs
//http://computervisionandjava.blogspot.my/2013/10/java-opencv-webcam.html

//For converting test files in images to video
//http://tsaith.github.io/combine-images-into-a-video-with-python-3-and-opencv-3.html
//looping through frame per frame approach
//http://opencv-java-tutorials.readthedocs.io/en/latest/04-opencv-basics.html#load-an-image-and-add-it-to-the-stream


//Inspiration from:
//https://udallascs.wordpress.com/2014/04/01/face-detection-with-a-webcam-using-only-opencv-with-java-and-not-javacv/
class mPanel extends JPanel{
    private static final long serialVersionUID = 1L;  
    private BufferedImage image;

    public mPanel(int camNo){
        super();
        String cam = "camera_" + camNo;
        TitledBorder border = new TitledBorder(cam);
        border.setTitleJustification(TitledBorder.CENTER);
        border.setTitlePosition(TitledBorder.TOP);        
        setBorder(border);
    }

    @Override 
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        if (this.image==null) return;
        g.drawImage(this.image,10,15,this.image.getWidth(),this.image.getHeight(), null);
        setSize(this.image.getWidth()+20,this.image.getHeight()+20);
    }

    public void refresh(Mat input){
        image=matToBufferedImage(input);
        repaint();
    }
    
    //Refered to:
    //https://github.com/opencv-java/video-basics/blob/master/src/it/polito/elite/teaching/cv/utils/Utils.java
    private BufferedImage matToBufferedImage(Mat original){
		// init
		BufferedImage image = null;
		int width = original.width(), height = original.height(), channels = original.channels();
		byte[] sourcePixels = new byte[width * height * channels];
		original.get(0, 0, sourcePixels);
		
		if (original.channels() > 1){
			image = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		}
		else{
			image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		}
		final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(sourcePixels, 0, targetPixels, 0, sourcePixels.length);
		
		return image;
    }

}
